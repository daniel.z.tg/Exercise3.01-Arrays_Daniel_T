import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.BiFunction;

import me.danielzgtg.compsci11_sem2_2017.common.datastructure.CommonLambdas;
import me.danielzgtg.compsci11_sem2_2017.common.ui.CommandDrivenCLI;
import me.danielzgtg.compsci11_sem2_2017.common.ui.ConsoleUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.LaunchUtils;

/**
 * A program to show the data from {@link AnalyzingData}.
 *
 * @author Daniel Tang
 * @since 10 May 2017
 */
@SuppressWarnings("unchecked")
public class AnalyzingData {

	/**
	 * The map of commands
	 */
	private static final Map<String, BiFunction<CommandDrivenCLI<RepetitionInData>, String[], RepetitionInData>>
			COMMANDS;

	static {
		final Map<String, BiFunction<CommandDrivenCLI<RepetitionInData>, String[], RepetitionInData>> commands =
				new HashMap<>();

		// Commands corresponding to backend methods
		commands.put("data", (cli, args) -> {
			final RepetitionInData state = cli.getState();

			state.displayArray();

			return state;
		});

		commands.put("reverse", (cli, args) -> {
			final RepetitionInData state = cli.getState();

			state.reverseArray();

			return state;
		});

		commands.put("frequency", (cli, args) -> {
			final RepetitionInData state = cli.getState();

			state.displayFrequency();

			return state;
		});

		commands.put("average", (cli, args) -> {
			final RepetitionInData state = cli.getState();

			state.displayAverage();

			return state;
		});

		// Quit command
		final BiFunction<CommandDrivenCLI<RepetitionInData>, String[], RepetitionInData> quitCommand =
				CommonLambdas.NULL_BIFUNCTION;

		commands.put("quit", quitCommand);
		commands.put("exit", quitCommand);

		// Post to static constant
		COMMANDS = Collections.unmodifiableMap(commands);
	}

	public static void main(final String[] args) {
		final Scanner scanner = new Scanner(System.in);

		LaunchUtils.loopingLaunchConsoleWrappedMainSafe((ignore) -> { // Loop-launch
			// Roll dice once per launch
			final RepetitionInData repetitionInData = new RepetitionInData(
					ConsoleUtils.promptIndex(scanner, "How many times to roll the die? (>= 1): "),
					ConsoleUtils.promptIndex(scanner, "How many sides on the die? (most dice have 6) (>= 1): "));

			// Start CLI with commands
			new CommandDrivenCLI<RepetitionInData>("> ", null, COMMANDS,
					repetitionInData, true).run();
		}, args);
	}
}
