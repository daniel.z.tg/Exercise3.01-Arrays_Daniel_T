import java.util.Random;

import me.danielzgtg.compsci11_sem2_2017.common.MathUtils;
import me.danielzgtg.compsci11_sem2_2017.common.StringUtils;
import me.danielzgtg.compsci11_sem2_2017.common.ui.ConsoleUtils;

import static me.danielzgtg.compsci11_sem2_2017.common.ui.PrintUtils.*;

/**
 * A simulator for a die.
 *
 * @author Daniel Tang
 * @since 10 May 2017
 */
@SuppressWarnings("WeakerAccess")
public class RepetitionInData {

	/**
	 * The default number of rolls on the die.
	 */
	public static final int DEFAULT_NUM_ROLLS = 10;

	/**
	 * The default number of sides on the die.
	 */
	public static final int DEFAULT_NUM_SIDES = 6;

	/**
	 * The threshold before the frequency displays as ordered pairs instead of a bar graph.
	 */
	public static final int MAX_SAFE_GRAPHING_DATASIZE = 1000;

	/**
	 * The values of die roll outcomes.
	 */
	private final /*mutable*/ int[] diceValues;

	/**
	 * The number of sides on the die.
	 */
	public final int numSides;

	/**
	 * Creates some new die roll data, using a usual die, the default number of times.
	 *
	 * @see RepetitionInData#DEFAULT_NUM_ROLLS
	 * @see RepetitionInData#DEFAULT_NUM_SIDES
	 */
	public RepetitionInData() {
		this(DEFAULT_NUM_ROLLS, DEFAULT_NUM_SIDES);
	}

	/**
	 * Creates some new die roll data, using a usual die, a specified number of times.
	 *
	 * @param numRolls The number of times to roll the die.
	 * @see RepetitionInData#DEFAULT_NUM_SIDES
	 */
	public RepetitionInData(final int numRolls) {
		this(numRolls, DEFAULT_NUM_SIDES);
	}


	/**
	 * Creates some new die roll data, a specified number of times, and sides.
	 *
	 * @param numRolls The number of times to roll the die.
	 * @param numSides The number if sides on the die.
	 */
	public RepetitionInData(final int numRolls, final int numSides) {
		this.numSides = numSides;
		this.diceValues = new int[numRolls];

		fillArray();
	}

	/**
	 * Fills the array of rolls
	 */
	private void fillArray() {
		final Random rng = new Random();

		for (int i = this.diceValues.length - 1; i >= 0; i--) {
			this.diceValues[i] = rng.nextInt(this.numSides) + 1;
		}
	}

	/**
	 * Dumps the roll data to the console.
	 */
	public void displayArray() {
		println(StringUtils.formatArray(this.diceValues, ", ", "{", "}"));
	}

	/**
	 * Displays the average of the rolls to the console.
	 */
	public void displayAverage() {
		formatln("The average of the rolls is %.4f", getAverage());
	}

	/**
	 * Calculates the mean of the rolls.
	 *
	 * @return The mean of the rolls.
	 */
	public double getAverage() {
		return MathUtils.arraySum(this.diceValues) / (double) this.diceValues.length;
	}

	/**
	 * Display the frequency of sides rolled to the console.
	 */
	public void displayFrequency() {
		final int[] frequencies = getFrequency();

		if (this.diceValues.length <= MAX_SAFE_GRAPHING_DATASIZE) {
			println(ConsoleUtils.graphArrayValues("%d | %s", frequencies, '*', 1,
					true, true));
		} else {
			println(ConsoleUtils.formatArrayValues("#%d's: %d", frequencies, true));
		}
	}

	/**
	 * Calculates the frequency of sides rolled.
	 *
	 * @return The frequency of sides rolled.
	 */
	public int[] getFrequency() {
		final int[] result = new int[this.numSides];

		for (int i = this.diceValues.length - 1; i >= 0; i--) {
			result[this.diceValues[i] - 1]++;
		}

		return result;
	}

	/**
	 * Reverses the array of rolls.
	 */
	public void reverseArray() {
		MathUtils.reverseArray(this.diceValues);
	}
}
